#### < WING_1 >
    **< Full_Fight, Phase_1, Split_1, Phase_2, Split_2, Phase_3 >**
        - VG
        - Gorseval

    **< Full_Fight, Phase_1, Kernan, Phase_2, Knuckles, Phase_3, Karde, Phase_4 >**
        - Sabetha

#### < WING_2 >
    **< Full_Fight, Phase_1, Phase_2, Phase_3, Phase_4, Phase_5, Phase_6 >**
        - Sloth
    
    **< Full_Fight, Ice_phase, Fire_phase, Storm_phase, Abomination_phase >**
        - Matthias

#### < WING_3 >
    **< Full_Fight, Phase_1, Pre-burn_1, Burn_1, Phase_2, Pre-burn_2, Burn_2, Phase_3, Pre-burn_3, Burn_3 >**
        - KC
    
    **< Full_Fight, Phase_1, Gliding, Phase_2 >**
        - Xera

#### < WING_4 >
    **< Full_Fight >**
        - Cairn

    **< Full_Fight, 100%-75%, 75%-50%, 50%-25%, 25%-0% >**
        - Mursaat
    
    **< Full_Fight, Phase_1, Split_1, Phase_2, Split_2, Phase_3 >**
        - Samarog
    
    **< Full_Fight, 100%-10%, Burst_1, Burst_2, Gambler, Thief, Burst_3, Burst_4, Burst_5, Drunkard, Burst_6, 10%-0%, Burst_7 >**
        - Deimos

#### < WING_5 >
    **< Full_Fight, Pre-Breakbar_1, Pre-Breakbar_2, Pre-Breakbar_3, Final >**
        - SH_Deesmina
    
    **< Full_Fight, Pre-Event, Main_fight, Dhuum_fight, Pre-Soulsplit_1, Soulsplit_1, Pre-Soulsplit_2, Soulsplit_2, Pre-Ritual, Shielded_Dhuum, Ritual >**
        - Dhuum
    
#### < WING_6 >
    **< Full_Fight, Left-Arm_phase, Burn_phase, Right-Arm_phase, Burn_phase, Left-Arm_phase, Burn_phase >**
        - CA
    
    **< Full_Fight, Nikare_P1, Kenut_P1, Nikare_P2, Kenut_P2, Nikare_P3, Kenut_P3 >**
        - Twin_Largos
    
    **< Full_Fight, Hydra, Qadim_P1, Apocalypse, Qadim_P2, Wyvern, Qadim_P3 >**
        - Qadim_1

#### < WING_7 >
    **< Full_Fight, Phase_1, Split_1, Phase_2, Split_2, Phase_3, Split_3, Phase_4 >**
        - Adina
    
    **< Full_Fight, Phase_1, Phase_2, Phase_3 >**
        - Sabir
    
    **< Full_Fight, Phase_1, Magma-Drop_1, Phase_2, Magma-Drop_2, Phase_3, North_Pylon, Phase_4, SouthWest_Pylon, Phase_5, SouthEast_Pylon, Phase_6 >**
        - Qadim_2

#### Phases-to-Use:

**Wing_1**
- **VG, Gorseval:** Phase_1, Phase_2, Phase_3
- **Sabetha:** Phase_1,Phase_2, Phase_3, Phase_4

**Wing_2**
- **Sloth:** Phase_1,Phase_2, Phase_3, Phase_4, Phase_5, Phase_6
- **Matthias:** Ice_phase, Fire_phase, Storm_phase, Abomination_phase

**Wing_3**
- **KC:** Phase_1, Phase_2, Phase_3
- **Xera:** Phase_1, Phase_2

**Wing_4**
- **Cairn:** Full_fight (divided by match time)
- **Mursaat:** 100%-75%, 75%-50%, 50%-25%, 25%-0%
- **Samarog:** Phase_1, Phase_2, Phase_3
- **Deimos:** 100%-10%, 10%-0%

**Wing_5**
- **SH_Deesmina:** Pre-Breakbar_1, Pre-Breakbar_2, Pre-Breakbar_3, Final
- **Dhuum:** Main_fight, Dhuum_fight, Ritual

**Wing_6**
- **CA:** All 3 Burn_phases
- **Twin_Largos:** Nikare_P1, Kenut_P1, Nikare_P2, Kenut_P2, Nikare_P3, Kenut_P3
- **Qadim_1:** Qadim_P1, Qadim_P2, Qadim_P3

**Wing_7**
- **Adina:** Phase_1,Phase_2, Phase_3, Phase_4
- **Sabir:** Phase_1, Phase_2, Phase_3
- **Qadim_2:** Phase_1,Phase_2, Phase_3, Phase_4, Phase_5, Phase_6